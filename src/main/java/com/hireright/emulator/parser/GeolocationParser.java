package com.hireright.emulator.parser;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.model.request.GeoLocationRequest;
import com.hireright.emulator.model.response.GeolocationResponse;
import com.hireright.emulator.parser.common.Parser;
import com.hireright.emulator.service.enums.HttpMethodType;
import com.hireright.services.geolocation.domain.Geolocation;
import com.hireright.services.geolocation.exception.GeolocationException;
import com.hireright.services.geolocation.service.GeolocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Ziya on 01.01.2020.
 */
@Component
public class GeolocationParser extends Parser<GeoLocationRequest, GeolocationResponse> {

    private final GeolocationService geolocationService;

    private static final Logger log = LoggerFactory.getLogger(GeolocationParser.class);

    @Autowired
    public GeolocationParser(GeolocationService geolocationService) {
        super(GeoLocationRequest.class);
        this.geolocationService = geolocationService;
    }

    @Override
    public GeolocationResponse execute(HttpServletRequest request, HttpMethodType methodType, String UUID) throws EmulatorException {
        if (methodType.equals(HttpMethodType.POST)) {
            return executePost(request, UUID);
        } else {
            throw new EmulatorException(ErrorList.UNSUPPORTED_METHOD_TYPE);
        }
    }

    private GeolocationResponse executePost(HttpServletRequest request, String UUID) throws EmulatorException {
        try {
            GeoLocationRequest geoLocationRequest = (GeoLocationRequest) parseRequestBody(request);
            log.info("UUID: {} - Request: {}", UUID, geoLocationRequest);
            String zipCode = geoLocationRequest.getZipCode();
            if (zipCode != null) {
                Geolocation coreResponse = geolocationService.getTimeZoneByZipCode(zipCode, UUID);
                return wrapServiceResponse(coreResponse);
            } else {
                throw new EmulatorException(ErrorList.REQUEST_PARSING_FAILED);
            }
        } catch (GeolocationException e) {
            throw new EmulatorException(e.getErrorList().getCode(), e.getErrorList().getMessage());
        }
    }

    private GeolocationResponse wrapServiceResponse(Geolocation coreResponse) {
        GeolocationResponse response = new GeolocationResponse();
        response.setTimeZone(coreResponse.getZoneId());
        response.setZipCode(coreResponse.getZipCode());
        return response;
    }

}

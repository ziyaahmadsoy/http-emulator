package com.hireright.emulator.parser;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.model.request.WeatherRequest;
import com.hireright.emulator.model.response.WeatherResponse;
import com.hireright.emulator.parser.common.Parser;
import com.hireright.emulator.service.enums.HttpMethodType;
import com.hireright.services.weather.domain.Weather;
import com.hireright.services.weather.exception.WeatherException;
import com.hireright.services.weather.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Ziya on 01.01.2020.
 */
@Component
public class WeatherParser extends Parser<WeatherRequest, WeatherResponse> {

    private final WeatherService weatherService;

    private static final Logger log = LoggerFactory.getLogger(WeatherParser.class);


    @Autowired
    public WeatherParser(WeatherService weatherService) {
        super(WeatherRequest.class);
        this.weatherService = weatherService;
    }

    @Override
    public WeatherResponse execute(HttpServletRequest request, HttpMethodType methodType, String UUID) throws EmulatorException {
        if (methodType.equals(HttpMethodType.GET)) {
            return executeGet(request, UUID);
        } else {
            throw new EmulatorException(ErrorList.UNSUPPORTED_METHOD_TYPE);
        }
    }

    private WeatherResponse executeGet(HttpServletRequest request, String UUID) throws EmulatorException {
        try {
            String cityName = request.getParameter("city-name");
            log.info("UUID: {} - Request: {}", UUID, cityName);
            Weather weather = weatherService.getWeatherInfoByCityName(cityName, UUID);
            WeatherResponse weatherResponse = new WeatherResponse();
            weatherResponse.setCityName(weather.getCityName());
            weatherResponse.setTemperature(weather.getTemperature());
            return weatherResponse;
        } catch (WeatherException e) {
            throw new EmulatorException(e.getErrorList().getCode(), e.getErrorList().getMessage());
        }
    }


}

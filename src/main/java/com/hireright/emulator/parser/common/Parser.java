package com.hireright.emulator.parser.common;

import com.google.gson.Gson;
import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.model.Request;
import com.hireright.emulator.model.Response;
import com.hireright.emulator.service.enums.ContentType;
import com.hireright.emulator.service.enums.HttpMethodType;
import org.springframework.http.HttpHeaders;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;


/**
 * Created by Ziya on 01.01.2020.
 */
public abstract class Parser<Req extends Request, Res extends Response> {

    private final Class<Req> clazz;

    protected Parser(Class<Req> clazz) {
        this.clazz = clazz;
    }

    public abstract Res execute(HttpServletRequest request, HttpMethodType methodType, String UUID) throws EmulatorException;

    private String requestToString(HttpServletRequest request) throws IOException {
        final Writer writer = new StringWriter();

        char[] buffer = new char[1024];
        try (ServletInputStream stream = request.getInputStream()) {
            Reader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        }

        return writer.toString();
    }

    protected Req parseRequestBody(HttpServletRequest request) throws EmulatorException {
        ContentType contentType = ContentType.getByDefinition(request.getHeader(HttpHeaders.CONTENT_TYPE));
        if (contentType.equals(ContentType.XML)) {
            return parseXMLRequest(request);
        } else if (contentType.equals(ContentType.JSON)) {
            return parserJSONRequest(request);
        }else throw new EmulatorException(ErrorList.REQUEST_PARSING_FAILED);
    }

    private Req parserJSONRequest(HttpServletRequest servletRequest) throws EmulatorException {
        Gson gson = new Gson();
        try {
            Req request = gson.fromJson(requestToString(servletRequest), clazz);
            return request;
        } catch (IOException e) {
            e.printStackTrace();
            throw new EmulatorException(ErrorList.REQUEST_PARSING_FAILED);
        }
    }

    private Req parseXMLRequest(HttpServletRequest servletRequest) throws EmulatorException {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(
                    clazz);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            final StringReader reader = new StringReader(requestToString(servletRequest));
            return (Req) jaxbUnmarshaller.unmarshal(reader);
        } catch (JAXBException | IOException ex) {
            throw new EmulatorException(ErrorList.REQUEST_PARSING_FAILED);
        }
    }

}


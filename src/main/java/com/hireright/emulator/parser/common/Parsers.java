package com.hireright.emulator.parser.common;

import com.hireright.emulator.config.ApiList;
import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.parser.GeolocationParser;
import com.hireright.emulator.parser.WeatherParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ziya on 01.01.2020.
 */
@Component
public class Parsers {

    private static final Map<ApiList, Class<? extends Parser>> PARSERS = new HashMap<>();

    static {
        PARSERS.put(ApiList.WEATHER_API, WeatherParser.class);
        PARSERS.put(ApiList.GEOLOCATION_API, GeolocationParser.class);
    }

    private final ApplicationContext applicationContext;

    @Autowired
    public Parsers(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public Parser getParser(ApiList api) throws EmulatorException {
        final Class<? extends Parser> parserClass = PARSERS.get(api);
        if (parserClass == null) throw new EmulatorException(ErrorList.PARSER_NOT_FOUND);
        return this.applicationContext.getBean(parserClass);
    }

}

package com.hireright.emulator.servlet;

import com.google.gson.Gson;
import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.model.Response;
import com.hireright.emulator.model.response.ErrorRes;
import com.hireright.emulator.service.enums.ContentType;
import com.hireright.emulator.service.DispatcherService;
import com.hireright.emulator.service.ParserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Ziya on 01.01.2020.
 */
public class EmulatorServlet extends HttpServlet {

    @Autowired
    private  DispatcherService dispatcherService;

    @Autowired
    private ParserService parserService;

    private static final Logger log = LoggerFactory.getLogger(EmulatorServlet.class);


    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req,
                          HttpServletResponse resp) throws ServletException, IOException {
        execute(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req,
                          HttpServletResponse resp) throws ServletException, IOException {
        execute(req,resp);
    }

    private void execute(HttpServletRequest req,
                          HttpServletResponse resp) throws ServletException, IOException    {

        final UUID uuid = UUID.randomUUID();

        ContentType responseType;

        try {
            responseType = ContentType.getByDefinition(req.getHeader(HttpHeaders.ACCEPT));
            Response dispatcherResponse = dispatcherService.redirectRequest(req,uuid.toString());
            log.info("UUID: {} - Response: {}",uuid,dispatcherResponse);
            if(responseType.equals(ContentType.XML)){
                //wrap to XML response
                writeXMLToResponse(resp,parserService.responseParser(dispatcherResponse));
            }else if(responseType.equals(ContentType.JSON)) {
                //wrap to JSON response
                writeJSONToResponse(resp,dispatcherResponse);
            }
        } catch (EmulatorException e) {
            /*marshal xml object to string*/
            try {
                writeXMLToResponse(resp,parserService.responseParser(wrapError(e)));
                log.info("UUID: {} - Response: {}",uuid,wrapError(e));
            } catch (EmulatorException e1) {
                e1.printStackTrace();
            }

        }
    }


    private void writeXMLToResponse(HttpServletResponse response, String data) throws IOException {
        response.reset();
        response.setContentType("text/xml;charset=UTF-8");
        response.getWriter().print(data);
        response.getWriter().flush();
        response.getWriter().close();
    }

    private void writeJSONToResponse(HttpServletResponse response, Response dispatcherResponse) throws IOException {
        response.reset();
        String jsonString = new Gson().toJson(dispatcherResponse);
        response.setContentType("application/json   ");
        response.getWriter().print(jsonString);
        response.getWriter().flush();
        response.getWriter().close();
    }

    private ErrorRes wrapError(Exception ex){
        ErrorRes errorRes;
        if(ex instanceof EmulatorException){
            EmulatorException emulatorException = (EmulatorException)ex;
            errorRes = new ErrorRes(emulatorException.getErrorCode(),emulatorException.getErrorMessage());
        }else {
            errorRes = new ErrorRes(ErrorList.UNKNOWN_ERROR);
        }
        return errorRes;
    }

}


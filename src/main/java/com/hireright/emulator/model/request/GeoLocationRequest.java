package com.hireright.emulator.model.request;

import com.hireright.emulator.model.Request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ziya on 01.01.2020.
 */

@XmlRootElement(name = "geolocation-request")
public class GeoLocationRequest implements Request {
    private String zipCode;

    public String getZipCode() {
        return zipCode;
    }

    @XmlElement(name = "zip-code")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "GeoLocationRequest{" +
                "zipCode='" + zipCode + '\'' +
                '}';
    }
}


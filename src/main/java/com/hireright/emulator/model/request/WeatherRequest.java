package com.hireright.emulator.model.request;

import com.hireright.emulator.model.Request;

/**
 * Created by Ziya on 01.01.2020.
 */
public class WeatherRequest implements Request {
    private String cityName;

    public WeatherRequest() {
    }

    public WeatherRequest(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return "WeatherRequest{" +
                "cityName='" + cityName + '\'' +
                '}';
    }
}

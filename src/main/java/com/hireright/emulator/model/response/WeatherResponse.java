package com.hireright.emulator.model.response;

import com.hireright.emulator.model.Response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ziya on 01.01.2020.
 */

@XmlRootElement(name = "weather-info")
public class WeatherResponse implements Response {

    private String cityName;

    private double temperature;

    public String getCityName() {
        return cityName;
    }

    @XmlElement(name = "city-name")
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public double getTemperature() {
        return temperature;
    }

    @XmlElement(name = "temperature")
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "WeatherResponse{" +
                "cityName='" + cityName + '\'' +
                ", temperature=" + temperature +
                '}';
    }
}

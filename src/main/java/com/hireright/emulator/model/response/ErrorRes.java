package com.hireright.emulator.model.response;

import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.model.Response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ziya on 01.01.2020.
 */
@XmlRootElement(name = "error")
public class ErrorRes implements Response {

    @XmlElement(name = "code")
    private long errorCode;

    @XmlElement(name = "message")
    private String errorMessage;

    public ErrorRes() {
    }

    public ErrorRes(long errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ErrorRes(ErrorList errorList) {
        this.errorCode = errorList.getErrorCode();
        this.errorMessage = errorList.getErrorMessage();
    }

    public long getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ErrorRes{" +
                "errorCode=" + errorCode +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}

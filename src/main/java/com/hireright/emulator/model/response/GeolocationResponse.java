package com.hireright.emulator.model.response;

import com.hireright.emulator.model.Response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ziya on 01.01.2020.
 */
@XmlRootElement(name = "geolocation")
public class GeolocationResponse implements Response {

    private String zipCode;
    private String timeZone;

    public String getZipCode() {
        return zipCode;
    }

    @XmlElement(name = "zip-code")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTimeZone() {
        return timeZone;
    }

    @XmlElement(name = "timezone")
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public String toString() {
        return "GeolocationResponse{" +
                "zipCode='" + zipCode + '\'' +
                ", timeZone='" + timeZone + '\'' +
                '}';
    }
}

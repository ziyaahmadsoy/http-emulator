package com.hireright.emulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ServletComponentScan
@SpringBootApplication
@EntityScan({"com.hireright.emulator","com.hireright.services.weather.domain","com.hireright.services.geolocation.domain"})
@EnableJpaRepositories({"com.hireright.services.weather.repository","com.hireright.services.geolocation.repository"})
public class EmulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmulatorApplication.class, args);
	}

}

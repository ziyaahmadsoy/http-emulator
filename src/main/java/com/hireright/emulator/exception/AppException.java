package com.hireright.emulator.exception;

/**
 * Created by Ziya on 3.1.2020.
 */
public class AppException extends Exception {

    private long errorCode;
    private String errorMessage;

    public AppException(long errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

package com.hireright.emulator.exception;

/**
 * Created by Ziya on 01.01.2020.
 */
public class EmulatorException extends Exception {

    private long errorCode;
    private String errorMessage;

    public EmulatorException(ErrorList errorList) {
        super(errorList.getErrorMessage());
        this.errorCode = errorList.getErrorCode();
        this.errorMessage = errorList.getErrorMessage();
    }

    public EmulatorException(long errorCode, String errorMessage){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}

package com.hireright.emulator.exception;

/**
 * Created by Ziya on 01.01.2020.
 */
public enum ErrorList {
    REQUEST_PARSING_FAILED(5001,"One or more mandatory fields are missing"),
    RESPONSE_PARSING_FAILED(5002,"Failed to parse response"),
    INVALID_PARAMETER_LENGTH(5003,"One or more parameter's length is invalid"),
    PARSER_NOT_FOUND(5004,"No any parser found for mentioned request type"),
    UNKNOWN_CONTENT_TYPE(5005,"Unknown content type"),
    UNSUPPORTED_METHOD_TYPE(5005,"Unsupported method type for API"),
    UNRECOGNIZED_URL(5006,"Unrecognized request url"),
    UNKNOWN_ERROR(5007,"UNKNOWN ERROR occured. Please contact with system administrator")
    ;
    private long errorCode;
    private String errorMessage;

    ErrorList(long errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}


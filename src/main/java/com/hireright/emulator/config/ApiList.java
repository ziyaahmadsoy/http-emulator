package com.hireright.emulator.config;


import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;

/**
 * Created by Ziya on 01.12.2019.
 */
public enum ApiList {
    WEATHER_API("/weather"),
    GEOLOCATION_API("/geolocation");

    private String requestUri;

    public String getRequestUri() {
        return requestUri;
    }

    ApiList(String requestUri) {
        this.requestUri = requestUri;
    }

    public static ApiList getApiByPath(String path) throws EmulatorException {
        for (ApiList ap: ApiList.values()) {
            if(ap.getRequestUri().equals(path))
                return ap;
        }
        throw new EmulatorException(ErrorList.UNRECOGNIZED_URL);
    }

}

package com.hireright.emulator.config;

import com.hireright.emulator.servlet.EmulatorServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServlet;
import java.util.Arrays;

/**
 * Created by Ziya on 01.01.2020.
 */
@Configuration
@ComponentScan(value = {"com.hireright.emulator.servlet", "com.hireright.services"})
public class WebConfig {

    @Bean
    public ServletRegistrationBean<HttpServlet> getEmulator() {
        ServletRegistrationBean<HttpServlet> bean = new ServletRegistrationBean<>();
        bean.setServlet(new EmulatorServlet());
        bean.setUrlMappings(Arrays.asList("/emulator/*"));
        return bean;
    }

}

package com.hireright.emulator.service;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.model.Response;

/**
 * Created by Ziya on 01.01.2020.
 */
public interface ParserService {
    String responseParser(Response response) throws EmulatorException;
}

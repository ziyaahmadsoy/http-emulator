package com.hireright.emulator.service.enums;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;

/**
 * Created by Ziya on 01.01.2020.
 */
public enum ContentType {
    XML("application/xml"),
    JSON("application/json");
    private String definition;

    ContentType(String definition) {
        this.definition = definition;
    }

    public String getDefinition() {
        return definition;
    }

    public static ContentType getByDefinition(String definition) throws EmulatorException {
        for (ContentType c: ContentType.values()) {
            if(c.getDefinition().equals(definition))
                return c;
        }
        throw new EmulatorException(ErrorList.UNKNOWN_CONTENT_TYPE);
    }
}

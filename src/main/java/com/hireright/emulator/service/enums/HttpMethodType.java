package com.hireright.emulator.service.enums;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;

/**
 * Created by Ziya on 01.01.2020.
 */
public enum HttpMethodType {
    GET,POST;

    public static HttpMethodType getMethodTypeByName(String name) throws EmulatorException {
        for (HttpMethodType h: HttpMethodType.values()) {
            if(valueOf(name).equals(h)){
                return h;
            }
        }
        throw new EmulatorException(ErrorList.UNSUPPORTED_METHOD_TYPE);
    }
}

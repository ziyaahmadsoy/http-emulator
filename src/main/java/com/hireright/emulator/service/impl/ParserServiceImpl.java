package com.hireright.emulator.service.impl;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.exception.ErrorList;
import com.hireright.emulator.model.Response;
import com.hireright.emulator.model.response.ErrorRes;
import com.hireright.emulator.model.response.GeolocationResponse;
import com.hireright.emulator.model.response.WeatherResponse;
import com.hireright.emulator.service.ParserService;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

/**
 * Created by Ziya on 01.01.2020.
 */
@Service
public class ParserServiceImpl implements ParserService {

    @Override
    public String responseParser(Response xmlResponse) throws EmulatorException {
        try {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(
                    GeolocationResponse.class,
                    WeatherResponse.class,
                    ErrorRes.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(xmlResponse, sw);

            //Return XML Content
            return sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
            throw new EmulatorException(ErrorList.RESPONSE_PARSING_FAILED);
        }
    }
}

package com.hireright.emulator.service.impl;

import com.hireright.emulator.config.ApiList;
import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.model.Response;
import com.hireright.emulator.parser.common.Parser;
import com.hireright.emulator.parser.common.Parsers;
import com.hireright.emulator.service.DispatcherService;
import com.hireright.emulator.service.enums.HttpMethodType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Ziya on 01.01.2020.
 */
@Service
public class DispatcherServiceImpl implements DispatcherService {

    private final Parsers parsers;

    private static final Logger log = LoggerFactory.getLogger(DispatcherServiceImpl.class);

    @Autowired
    public DispatcherServiceImpl(Parsers parsers) {
        this.parsers = parsers;
    }

    @Override
    public Response redirectRequest(HttpServletRequest servletRequest, String uuid) throws EmulatorException {

        String servletPath = servletRequest.getPathInfo();
        HttpMethodType methodType = HttpMethodType.getMethodTypeByName(servletRequest.getMethod());
        log.info("UUID: {} - Request Path: {}, Request Method: {}",uuid,servletPath,methodType);
        final Parser parser = parsers.getParser(ApiList.getApiByPath(servletPath));
        Response response = parser.execute(servletRequest,methodType,uuid);

        return response;
    }


}

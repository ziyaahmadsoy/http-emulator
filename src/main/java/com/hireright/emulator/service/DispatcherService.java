package com.hireright.emulator.service;

import com.hireright.emulator.exception.EmulatorException;
import com.hireright.emulator.model.Response;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Ziya on 01.01.2020.
 */
public interface DispatcherService {
    Response redirectRequest(HttpServletRequest servletRequest, String uuid) throws EmulatorException;
}


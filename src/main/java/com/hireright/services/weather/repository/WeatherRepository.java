package com.hireright.services.weather.repository;

import com.hireright.services.weather.domain.Weather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ziya on 01.01.2020.
 */
@Repository
public interface WeatherRepository extends JpaRepository<Weather,Long> {

    Weather findOneByCityName(String cityName);

}

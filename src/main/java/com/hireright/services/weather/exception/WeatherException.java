package com.hireright.services.weather.exception;

/**
 * Created by Ziya on 27.12.2019.
 */
public class WeatherException extends Exception {

    private WeatherErrorList errorList;

    public WeatherException(WeatherErrorList errorList) {
        this.errorList = errorList;
    }

    public WeatherErrorList getErrorList() {
        return errorList;
    }


    @Override
    public String toString() {
        return "WeatherException{" +
                "errorList=" + errorList +
                '}';
    }
}

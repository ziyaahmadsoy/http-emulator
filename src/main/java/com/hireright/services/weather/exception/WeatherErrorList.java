package com.hireright.services.weather.exception;

/**
 * Created by Ziya on 01.01.2020.
 */
public enum WeatherErrorList {
    NOT_FOUND(404,"Country not found for mentioned name")
    ;
    private long code;
    private String message;

    WeatherErrorList(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "WeatherErrorList{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}

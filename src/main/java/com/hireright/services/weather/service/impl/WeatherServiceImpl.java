package com.hireright.services.weather.service.impl;

import com.hireright.services.weather.domain.Weather;
import com.hireright.services.weather.exception.WeatherErrorList;
import com.hireright.services.weather.exception.WeatherException;
import com.hireright.services.weather.repository.WeatherRepository;
import com.hireright.services.weather.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Ziya on 01.01.2020.
 */
@Service
public class WeatherServiceImpl implements WeatherService {

    private final WeatherRepository repository;

    private static final Logger log = LoggerFactory.getLogger(WeatherServiceImpl.class);


    @Autowired
    public WeatherServiceImpl(WeatherRepository repository) {
        this.repository = repository;
    }


    @Override
    public Weather getWeatherInfoByCityName(String cityName, String UUID) throws WeatherException {
        log.info("UUID: {} Request to get temperature by city name: {}", UUID, cityName);
        Weather weather = repository.findOneByCityName(cityName.toUpperCase());
        if (weather != null) {
            log.info("UUID: {} SUCCESSFUL Response: {}", UUID, weather);
            return weather;
        } else {
            log.info("UUID: {} No data found for city name: {}", UUID, cityName);
            throw new WeatherException(WeatherErrorList.NOT_FOUND);
        }
    }

}

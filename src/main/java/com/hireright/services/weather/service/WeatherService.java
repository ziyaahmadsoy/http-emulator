package com.hireright.services.weather.service;

import com.hireright.services.weather.domain.Weather;
import com.hireright.services.weather.exception.WeatherException;

/**
 * Created by Ziya on 01.01.2020.
 */
public interface WeatherService {

    Weather getWeatherInfoByCityName(String cityName, String UUID) throws WeatherException;
}

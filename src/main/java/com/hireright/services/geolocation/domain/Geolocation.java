package com.hireright.services.geolocation.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZoneId;

/**
 * Created by Ziya on 01.01.2020.
 */
@Entity(name = "GEOLOCATION")
public class Geolocation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String zipCode;

    @NotNull
    private String zoneId;

    public Geolocation() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public String toString() {
        return "Geolocation{" +
                "id=" + id +
                ", zipCode='" + zipCode + '\'' +
                ", zoneId='" + zoneId + '\'' +
                '}';
    }
}

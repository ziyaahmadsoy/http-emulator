package com.hireright.services.geolocation.exception;

/**
 * Created by Ziya on 01.01.2020.
 */
public class GeolocationException extends Exception {
    private GeolocationErrorList errorList;

    public GeolocationException(GeolocationErrorList errorList) {
        this.errorList = errorList;
    }

    public GeolocationErrorList getErrorList() {
        return errorList;
    }

    public void setErrorList(GeolocationErrorList errorList) {
        this.errorList = errorList;
    }

    @Override
    public String toString() {
        return "GeolocationException{" +
                "errorList=" + errorList +
                '}';
    }
}

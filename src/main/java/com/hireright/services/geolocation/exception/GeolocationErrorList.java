package com.hireright.services.geolocation.exception;

/**
 * Created by Ziya on 01.01.2020.
 */
public enum GeolocationErrorList {
    NOT_FOUND(404,"Country not found for zip code")
    ;
    private long code;
    private String message;

    GeolocationErrorList(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "GeolocationErrorList{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}

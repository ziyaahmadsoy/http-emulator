package com.hireright.services.geolocation.repository;

import com.hireright.services.geolocation.domain.Geolocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ziya on 01.01.2020.
 */
@Repository
public interface GeolocationRepository extends JpaRepository<Geolocation,Long> {

    Geolocation findOneByZipCode(String zipCode);
}

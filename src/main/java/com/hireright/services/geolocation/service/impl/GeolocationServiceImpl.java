package com.hireright.services.geolocation.service.impl;

import com.hireright.services.geolocation.domain.Geolocation;
import com.hireright.services.geolocation.exception.GeolocationErrorList;
import com.hireright.services.geolocation.exception.GeolocationException;
import com.hireright.services.geolocation.repository.GeolocationRepository;
import com.hireright.services.geolocation.service.GeolocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Ziya on 01.01.2020.
 */

@Service
public class GeolocationServiceImpl implements GeolocationService {

    private final GeolocationRepository repository;

    private static final Logger log = LoggerFactory.getLogger(GeolocationServiceImpl.class);


    @Autowired
    public GeolocationServiceImpl(GeolocationRepository repository) {
        this.repository = repository;
    }


    @Override
    public Geolocation getTimeZoneByZipCode(String zipCode, String UUID) throws GeolocationException {
        log.info("UUID: {} Request to get city name by zip code: {}",UUID,zipCode);
        Geolocation geolocation = repository.findOneByZipCode(zipCode);

        if (geolocation != null) {
            log.info("UUID: {} SUCCESSFUL Response: {}", UUID, geolocation);
            return geolocation;
        } else {
            log.info("UUID: {} No data found for zipcode: {}",UUID,zipCode);
            throw new GeolocationException(GeolocationErrorList.NOT_FOUND);
        }
    }
}

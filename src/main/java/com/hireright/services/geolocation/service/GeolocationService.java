package com.hireright.services.geolocation.service;

import com.hireright.services.geolocation.domain.Geolocation;
import com.hireright.services.geolocation.exception.GeolocationException;

/**
 * Created by Ziya on 01.01.2020.
 */
public interface GeolocationService {

    Geolocation getTimeZoneByZipCode(String zipCode, String UUID) throws GeolocationException;
}
